import wave
import struct
from math import sin, pi


def write_file(filename, func):
    seconds = 5
    framerate = 41000

    with wave.open(filename, 'wb') as f:
        f.setnchannels(1)
        f.setsampwidth(2)
        f.setframerate(framerate)

        for i in range(seconds * framerate):
            f.writeframes(struct.pack("h", func(i, framerate)))


def sin_fn(x, const, framerate):
    return int(sin(x * 150 * const * (pi * 2) / framerate) * (2**(16-1) - 1))


def abs_fn(x, fr):
    frequency = 500
    return int(
        abs(
            ((x * frequency) % 65536) - (65535/2)
        )
    )


def combined(c1, c2):
    return lambda x, f: int((sin_fn(x, c1, f) + sin_fn(x, c2, f)) / 2)


def main():
    write_file("abs_x.wav", abs_fn)


if __name__ == '__main__':
    main()
